#MR Form GCLID Cookie
####Gravity Forms and CF7 GCLID prefills

Drops GCLID values encountered in a URI into a cookie for use in Gravity Forms or Contact Form 7 form prefills

##Prerequisites

####Gravity Forms

* None - runs out of the box

####Contact Form 7

* ***[Contact Form 7 Modules](https://wordpress.org/plugins/contact-form-7-modules/ "Contact Form 7 Modules")*** is required to add hidden fields to Contact Form 7 forms

###Usage

###Gravity Forms

1. Add your GCLID field to Gravity Forms.  The most common use will be as a hidden field, but it can be a text field or textarea if you're doing something exotic.
2. Edit the field, and in the 'Advanced' tab
 * Check the ***Allow field to be populated dynamically*** checkbox
 * Set the ***Parameter Name*** field to `gclid`

Once done, and you have activated this plugin, you should be good to go.

###Contact Form 7

1. Select ***Hidden Field*** from the ***Generate Tag*** drop box (no hidden field option?  See the *Prerequisites*)
2. Name the field anything you like, set its ID to anything you like (or nothing at all)
3. Set the field's ***Default value*** to `prefill-gclid`
4. Add the field shortcode to the form, and the value shortcode to your notifications

Once done, and you have activated this plugin, you should be good to go.

##Testing

###Cookie

Append a GCLID value to your URL using the `gclid` querystring identifier and any value, load your page, and inspect your cookies.  There should be a `mr-form-gclid-cookie` cookie with the value you set.

###Form Prefills

#####Via GET value

Append a GCLID value to your URL using the `gclid` querystring identifier and any value, load your page, and inspect the generated form code.  The field you have nominated as a GCLID field should have its value prepopulated with the value you set.

#####Via COOKIE value

Do the GET value test above, then visit the page containing the form *without* a `gclid` querystring appended and inspect the generated form code.  The field you have nominated as a GCLID field should have its value prepopulated with the value from the `mr-form-gclid-cookie' value.

##Caveats

1. The `gclid` GET value will *always* override the `mr-form-gclid-cookie` value.  This means if a `gclid` GET value is present then it will always be used to prefill the nominated fields, and it will always overwrite the `mr-form-gclid-cookie` cookie.
2. The `mr-form-gclid-cookie` cookie is set to expire a month after it is created.  Whenever it is overwritten by a new `gclid` value, the expiry is set to a month after the overwrite.

##Changelog
* 1.0  - Initial Release - forked from the ***[mr-gf-gclid-cookie](https://bitbucket.org/mresults/mr-gf-gclid-cookie "MR Gravity Forms GCLID Cookie")*** plugin, which was *Gravity Forms* centric.

##License
This plugin is developed and maintained by ***[Shaun Johnston](http://about.me/shaun.johnston "Shaun Johnston") at ***[Marketing Results](https://www.marketingresults.com.au "Marketing Results").
