<?php
/*
Plugin Name: MR Forms GCLID
Plugin URI: https://bitbucket.org/mresults/mr-form-gclid-cookie
Description: Drops GCLID and UTM values encountered in a URI into a cookie for use in Gravity Forms or Contact Form 7 form prefills
Author: Shaun Johnston, Marketing Results <shaun@marketingresults.com.au>
Version: 2.0
Author URI: http://about.me/shaun.johnston
*/

// Captures a GCLID passed in a URI, and stores it in a cookie
function mr_form_gclid_cookie_capture() {
	
	if (isset($_GET['gclid'])) {
		
		// Set a cookie containing the GCLID value, to expire in one month
		setcookie('mr-form-gclid-cookie', $_GET['gclid'], strtotime('+1 month'));

	}
  
  mr_form_gclid_get_utms();
  
}

// Get all UTM values, while adding them to a cookie for later use
function mr_form_gclid_get_utms() {
  $utm = 
    (isset($_COOKIE['mr-form-utm-cookie'])) 
      ? unserialize($_COOKIE['mr-form-utm-cookie']) 
      : array(
        'utm_source' => '',
        'utm_medium' => '',
        'utm_content' => '',
        'utm_campaign' => '',
      )
  ;
  
  foreach ($_GET as $key => $val) {
    $name = explode('_', $key);
    if (strtolower($name[0]) == 'utm') {
      $utm[$name[1]] = $val;
    }
  }
  
  setcookie('mr-gf-utm-cookie', serialize($utm), strtotime('+1 month'));
  
  return $utm;
  
}

// Sifts a Gravity Forms form for fields set to dynamically prefill with a gclid placeholder
// and prefills qualifying fields with either a GCLID in the GET request, or a stored GCLID valud
function mr_form_gclid_cookie_prefill_gf($form) {

  $utms = mr_form_gclid_get_utms();
	
	// Iterate over the field list, using each as a reference that can be written back to
	foreach ($form['fields'] as &$field) {
		
		// Check for some qualifiers that determine this field should be modified
		if (
			isset($field['allowsPrepopulate']) &&
			$field['allowsPrepopulate'] &&
			isset($field['inputName']) &&
			$field['inputName'] == 'gclid'
		) {
			
			// Set the field default from either the GET request GCLID or a stored GCLID 
			$field['defaultValue'] = (isset($_GET['gclid'])) 
				? $_GET['gclid'] 
				: (isset($_COOKIE['mr-form-gclid-cookie']))
					? $_COOKIE['mr-form-gclid-cookie']
					: '';
		}
    
    foreach ($utms as $key => $val) {
      // Check for some qualifiers that determine this field should be modified
      if (
        isset($field['allowsPrepopulate']) &&
        $field['allowsPrepopulate'] &&
        isset($field['inputName']) &&
        $field['inputName'] == "utm-{$key}"
      ) {
        
        // Set the field default from either the GET request GCLID or a stored GCLID 
        $field['defaultValue'] = $val;
      }      
    
	  }
    
	}
	
	// Send the form back to render
	return $form;
}

// Transform any CF7 hidden field values with set to 'prefill-gclid' to the actual GCLID
function mr_form_gclid_cookie_prefill_cf7($value) {
 
  $utms = mr_form_gclid_get_utms();
  
  // If the value is what we're looking for ...
  if ($value == 'prefill-gclid') {
    
    // Set the value to either the GET request GCLID or a stored GCLID
    if (isset($_GET['gclid']) && $_GET['gclid']) {
      $value = $_GET['gclid'];
    }
    elseif (isset($_COOKIE['mr-form-gclid-cookie']) && $_COOKIE['mr-form-gclid-cookie']) {
      $value = $_COOKIE['mr-form-gclid-cookie'];
    }
    else {
      $value = '';
    }
  }
  
  foreach ($utms as $key => $val) {
    // Check for some qualifiers that determine this field should be modified
    if ($value == "prefill-utm-{$key}") {
      
      // Set the field default from either the GET request GCLID or a stored GCLID 
      $value = $val;
    }      
    
	}  
  
  // Send the value back to the form
  return $value;
}

// Capture the GCLID on initialisation
add_action('init', 'mr_form_gclid_cookie_capture');

// Prefill any Gravity Forms fields set to be dynamically set with the GCLID field on form render
add_action('gform_pre_render', 'mr_form_gclid_cookie_prefill_gf');

// Prefill any Contact Form 7 hidden fields set to 'prefill-gclid' with the actual GCLID
add_filter('wpcf7_hidden_field_value', 'mr_form_gclid_cookie_prefill_cf7');